package shapes;
import static org.junit.Assert.*; 
import org.junit.Test;

public class SphereTest {
    /**
     * Tests
     * 
     * @exception IllegalArgumentException
     * 
     * @author Alexander Roxas
     */
    @Test
    public void testRadius(){
        Sphere s = new Sphere(7);
        assertEquals(7, s.getRadius(), 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_negativeValue_throwsException(){
        Sphere s = new Sphere(-7);
    }

    @Test
    public void testVolume(){
        Sphere s = new Sphere(7);
        assertEquals(1436.76, s.getVolume(), 0.01);
    }

    @Test
    public void testSurfaceArea(){
        Sphere s = new Sphere(7);
        assertEquals(615.75, s.getSurfaceArea(), 0.01);
    }

    @Test
    public void testToString(){
        Sphere s = new Sphere(7);
        assertEquals("This sphere has a radius of: 7.0", s.toString());
    }
}
