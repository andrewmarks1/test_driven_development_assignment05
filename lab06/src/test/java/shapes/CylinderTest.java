package shapes;

import static org.junit.Assert.*;
import org.junit.Test;

<<<<<<< HEAD
=======
 /**
     * @exception UnsupportedOperationException
     * 
     * @author Andrew Marks
     */
>>>>>>> 60cb9829c2777260ff242fc48dbb98a344a202a3

public class CylinderTest {

    /**
     * Tests
     * 
     * @author Andrew Marks
     */

    @Test(expected = IllegalArgumentException.class)
    public void test_constructor_neg_values_throwsException(){
        Cylinder c = new Cylinder(-2, -7);
    }
    

    
    @Test
    public void testVolume(){
        Cylinder c = new Cylinder(2, 4);
        assertEquals(50.26, c.getVolume(), 0.01);
    }

    @Test
    public void returnGetSurfaceArea(){
        Cylinder c = new Cylinder(2, 4);
        assertEquals(75.39, c.getSurfaceArea(), 0.01);
    }

    @Test
    public void returnRadius(){
        Cylinder c = new Cylinder(2, 4);
        assertEquals(2, c.getRadius(), 0.01);
    }

    @Test
    public void returnHeight(){
        Cylinder c = new Cylinder(2, 4);
        assertEquals(4, c.getHeight(), 0.00);
    }

    @Test
    public void returnToString(){
        Cylinder c = new Cylinder(2, 4);
        assertEquals("This cylinder has a radius of 2.0, and a height of 4.0", c.toString()); 
    }
}
