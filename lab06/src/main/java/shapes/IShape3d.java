package shapes;

public interface IShape3d {
    

    double getVolume();
    double getSurfaceArea();
}
