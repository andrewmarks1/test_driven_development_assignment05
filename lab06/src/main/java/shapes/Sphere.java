package shapes;


public class Sphere implements IShape3d{
    
    /**
     * @exception IllegalArgumentException
     * 
     * @author Andrew Marks
     */

     private double radius;

    public Sphere(double radius){
        if(radius < 0){
            throw new IllegalArgumentException("Radius can't be a negative number");
        }
        else{
            this.radius = radius;
        }
    }

    public double getRadius(){
        return this.radius;
    }

    public double getSurfaceArea(){
        return (4 * Math.PI * Math.pow(this.radius, 2));
    }

    public double getVolume(){
        return ((4.0/3.0) * Math.PI * Math.pow(this.radius, 3));
    }

    public String toString(){
        return "This sphere has a radius of: " + this.radius;
    }
}
