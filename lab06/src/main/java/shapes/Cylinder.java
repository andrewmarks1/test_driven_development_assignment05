package shapes;

public class Cylinder implements IShape3d{

    /**
     * @exception IllegalArgumentException
     * 
     * @author Alexander Roxas
     */
    private double radius;
    private double height;

    public Cylinder(double radius, double height){
        if (radius < 0 || height < 0){
            throw new IllegalArgumentException("Parameters cannot be a negative number");
        }
        this.radius = radius;
        this.height = height;
    }

    public double getVolume(){
        return Math.PI * Math.pow(this.radius, 2) * this.height;
    }

    public double getSurfaceArea(){
        return (2 * Math.PI * Math.pow(radius, 2)) + (2 * Math.PI * this.radius * this.height); 
    }

    public double getRadius(){
        return this.radius;
    }

    public double getHeight(){
        return this.height;
    }
    
    public String toString(){
        return "This cylinder has a radius of " + this.radius + ", and a height of " + this.height;
    }
}
